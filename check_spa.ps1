﻿##############################################################################################
#
# Script to restart spa.exe and DsxPW.exe when spa.exe hangs.
#
##############################################################################################
#
# This script will check 2 conditions.
# 
# 2. Checks if spa.exe is not running at all and will then attempt to kill any
#    dsxpw processes and then proceed to start spa
# 1. It will check whether the process named spa.exe has stopped responding.  
#    If it has then it will kill spa.exe and dsxpw.exe and restart spa.exe which will 
#    automatically restart dsxpw.exe.
#
#
##############################################################################################
#
# Script written by Michael Pal on Nov 2nd 2011
# Modified by Dewey Liew, 2015-06-29
#
##############################################################################################

# Import send email and date time functions
  . .\functions\format_date.ps1

# Array Variable for storing error message
[array]$errorLog = @{}

#Send Email Settings
$email = [xml](gc .\etc\emailconfig.xml)

# function to kill processes
function killProcess ($process) {
	if ($process) {
		$process.kill()
	}
}

# Get process handles for spa.exe
$spa = gps spa -ErrorAction SilentlyContinue -ErrorVariable $spaErr

# 2015-06-29 INC0493757 DL
# If spa is not running, then start spa. Make sure
# to skip the not responding check.
if ($spa -eq $null) {
	$errDate = Get-Date
	$errorLog = "Process spa.exe not running as of $errDate`n"

	& "C:\Program Files\MKS\MKMS\spa.exe"
	$errorLog += "`nRestarted Process spa.exe at " + (Get-Date).toshorttimestring() +"`n"

}

# If the Responding property for spa.exe is $false then kill the processes and restart them
elseif ($spa.Responding -eq $false) {
	$errDate = Get-Date
	$errorLog = "Process spa.exe has stopped responding at approximately $errDate`n"
	
	killProcess($spa)
	$errorLog += "`nKilled Process spa.exe at " + (Get-Date).toshorttimestring() +"`n"
	
	sleep 5
	& "C:\Program Files\MKS\MKMS\spa.exe"
	$errorLog += "`nRestarted Process spa.exe at " + (Get-Date).toshorttimestring() +"`n"
	$errorLog += "`nGood Hunting!"
	
}

# If any error messages, email them
# Have to do this juggling to ensure that an error notification is NOT sent
# on empty errorLog --DL
[string]$body = ($errorLog) | out-string
if( $body -ne "" ) {
	$server = (gci env:computername).value
	[string]$subject = "Errors on $server."
	[string]$from = $email.email.sender
	[string[]]$to = $email.email.recipients

	# Powershell strangeness in send-mailmessage, Body cannot be null or empty 
	# or throws validation error. Have to pass a hash table instead for Body
	$bodyHashTbl = @{}
	if( ![String]::IsNullOrEmpty($body) ) {
		$bodyHashTbl['body']=$body
	}
	
	send-mailmessage -from $from -to $to -subject $subject -smtpServer $email.email.smtpServer @bodyHashTbl
}