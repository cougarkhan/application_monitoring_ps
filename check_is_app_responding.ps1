﻿##############################################################################################
#
# Script to restart user space application when it hangs.
#
##############################################################################################
#
# This script will check 1 condition.
# 
# 1. It will check whether the process has stopped responding.  
#    If it has then it will kill the process and restart the process.
#
##############################################################################################
#
# Script written by Michael Pal on Feb 17th 2012
#
##############################################################################################

[CmdletBinding()] param (
						[Parameter(Position=0,mandatory=$true)][string]$processName,
						[Parameter(Position=1,mandatory=$true)][string]$pathToApp,
						[Parameter(Position=2,mandatory=$false)][string]$argumentsForApp,
						[switch]$help
						)
# Import send email and date time functions
 . .\functions\send_email.ps1
 . .\functions\format_date.ps1
 
function fun_help() {
$helpText = @"

NAME: check_is_app_responding.ps1

The scripts will check if a process if running.  If the process
is running it will check if it is responding.  If the process is not
running or is running and not responding it will be resatrted.

Please note that to check if a application is responding, the script
must be run under the user session that started the application.

PARAMETERS:

-processName`t`tDisplay Name of process that is being checked.
-pathToApp`t`tFull path to the application to execute.
-argumentsForApps`t`tArgument list for applcaitions. (Optional)
`n 

"@
$helpText
exit
}

if ($help) { fun_help }

# Array Variable for storing error message
[array]$errorLog = @{}

# funciton to kill processes
function killProcess ($process) {
	if ($process) {
		$process.kill()
	}
}

# Get process handles for $processName
$process = gps $processName -ErrorAction SilentlyContinue -ErrorVariable $processErr


# If the Responding property for $processName is $false then kill the process and restart it
if ($process -eq $null) {
	$errorLog = "Process $process.Name has exited unexpectedly at approximately " + (Get-Date).toshorttimestring() + "`n"
	
	if ($argumentsForApp) {
		start-process $pathToApp -ArgumentList $argumentsForApp
	} else {
		start-process $pathToApp
	}
	
	$errorLog += "`nRestarted Process $process.Name at " + (Get-Date).toshorttimestring() + "`n"
	$errorLog += "`nGood Hunting!"
	
} elseif ($process -ne $null -AND $process.Responding -eq $false) {
	$errorLog = "Process $process.Name has stopped responding at approximately " + (Get-Date).toshorttimestring() + "`n"
	
	$process.kill()
	$errorLog += "`nKilled Process $process.Name at " + (Get-Date).toshorttimestring() + "`n"
	
	sleep 5
	
	if ($argumentsForApp) {
		start-process $pathToApp -ArgumentList $argumentsForApp
	} else {
		start-process $pathToApp
	}
	
	$errorLog += "`nRestarted Process $process.Name at " + (Get-Date).toshorttimestring() + "`n"
	$errorLog += "`nGood Hunting!"
	
	#send_email($errorLog)
	
}